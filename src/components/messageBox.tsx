import { Card } from "react-bootstrap";

type Post = {
  username: string;
  topic: string;
  message: string;
};

type MessageBox = {
  post: Post;
};

const MessageBox = (props: MessageBox) => {
  const post = props.post;

  console.log(post);

  return (
    <Card
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        width: "585px",
        height: "373px",
        border: "1px solid black",
        margin: "3px",
      }}
    >
      <Card.Body style={{display: "flex", width: "585px", height: "373px"}}>
        <div style={{backgroundColor: "#222", color: "white", paddingTop: "5px", width: "30px"}}>99</div>
        <div style={{backgroundColor: "#EEE", width: "100%"}}>
            right
        <Card.Title>{post.username}</Card.Title>
        <Card.Subtitle className="mb-2 text-muted">Topic: {post.topic}</Card.Subtitle>
        <Card.Text>Message: {post.message}</Card.Text>
        </div>
      </Card.Body>
    </Card>
  );
};

export default MessageBox;
