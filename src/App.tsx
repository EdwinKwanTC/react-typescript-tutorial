import MessageBox from "./components/messageBox";

type Post = {
  username: string;
  topic: string;
  message: string;
};

function App() {
  const posts: Post[] = [
    { username: "edwin", topic:"hello", message: "hello world" },
    { username: "man", topic: "learning react" ,message: "react is so hard!" },
    { username: "katy", topic: "troll" ,message: "I am a troll!" },
  ];

  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        height: "100%",
        flexDirection: "column",
        flex: "flex-wrap"
      }}
    >
      {posts.map((post) => (
        <MessageBox post={post} />
      ))}
    </div>
  );
}

export default App;
